<?php
include "../countries/header.php";
$id = $_GET['id'];

$city = City::find($id);

?>

    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h1><?php echo $city->getName(); ?></h1>

                <div>
                    Code: <?php echo $city->getCountry(); ?>
                </div>

                <?php if($city->getPopulation()) : ?>
                    <div>
                        Population: <?php echo $city->getPopulation(); ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="col-sm-4">
                <a href="delete.php?id=<?php echo $city->getID(); ?>" class="btn btn-danger">
                    Trinti
                </a>

                <a href="edit.php?id=<?php echo $city->getID(); ?>" class="btn btn-success">
                    Redaguoti
                </a>
            </div>
        </div>
    </div>

<?php include "../countries/footer.php"; ?>