<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "../countries/header.php";

$id = $_POST['id'];
$name = $_POST['name'];
$country = $_POST['country'];
$population = $_POST['population'];

$city = City::find($id);
$city->name = $name;
$city->country = $country;
$city->population = $population;

$city->save();