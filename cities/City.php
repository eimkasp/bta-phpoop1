<?php

class City {

    private $id;
    public $name;
    public $population;
    public $country;

    public static $count = 0;
    public static $all = [];

    function __construct($name, $population, $id, $country) {
        $this->name = $name;
        $this->population = $population;
        $this->id = $id;
        $this->country = $country;
        City::$count++;
    }

    function save() {

    	// Patikrinu ar mano miestas turi id
    	if($this->id != null) {
//    		echo "UPDATE city SET name = $this->name , country = $this->country , population = $this->population WHERE id = $this->id";
			MySQL::query("UPDATE city SET name = '$this->name' , country = '$this->country' , population = '$this->population' WHERE id = $this->id");
		} else {
			// jei id yra null tai sukursime nauja miesta duombazeje
			MySQL::query("INSERT into city (country, name, population) VALUES ('$this->country', '$this->name', '$this->population')");
		}
	}

    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

    function getPopulation() {
        return $this->population;
    }

    function getID() {
        return $this->id;
    }

    function getCountry() {
        return $this->country;
    }




	static function find($id) {
        $results = MySQL::select("SELECT * from city WHERE id = $id");
        $city = new City($results[0]['name'], $results[0]['population'], $results[0]['id'], $results[0]['country']);

        return $city;
	}

    static function getAll() {

        /* Prisijungiame prie duombazes */
        $results = MySQL::select("SELECT * from city");


        foreach ($results as $result) {
            // Kiekvienai grazintai mysql eilutei, sukuriame country tipo objekta
            $city = new City($result['name'], $result['population'], $result['id'], $result['country']);
            array_push(City::$all, $city);
        }

        return City::$all;
    }

    function delete() {
        $query = MySQL::delete("DELETE from city WHERE id = $this->id");
    }
}