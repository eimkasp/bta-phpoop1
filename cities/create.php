<?php

include "../countries/header.php";

$countries = Country::getAll();

?>

<div class="container">
	<div class="row">
		<div class="col-sm-6">
            <h1>Miesto pridejimas</h1>
			<form method="post" action="store.php">
                <div>
                    <input type="text" placeholder="Miesto pavadinimas" name="name" class="form-control" />
                </div>

                <div class="mt-3">
                    <input type="number" placeholder="Miesto popliacija" name="population" class="form-control" />
                </div>

                <!-- cia bus saliu selectas -->
                <select name="country" class="form-control mt-3">
                    <?php foreach($countries as $country) : ?>
                        <option value="<?php echo $country->getID(); ?>"><?php echo $country->getName(); ?></option>
                    <?php endforeach; ?>
                </select>

                <input type="submit" class="btn btn-success mt-3" value="Prideti miesta"/>
            </form>
		</div>
	</div>
</div>

<?php include "../countries/footer.php";?>
