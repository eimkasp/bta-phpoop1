<?php

include "../countries/header.php";

$countries = Country::getAll();

// miesto id
$id = $_GET['id'];

// gauname miesto objekta is city klases
$city = City::find($id);

?>

<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h1>Miesto redagavimas</h1>
			<form method="post" action="update.php">
				<div>
					<input type="text" value="<?php echo $city->getName(); ?>" placeholder="Miesto pavadinimas" name="name" class="form-control" />
				</div>

				<div class="mt-3">
					<input type="number" value="<?php echo $city->getPopulation(); ?>" placeholder="Miesto popliacija" name="population" class="form-control" />
				</div>

				<!-- cia bus saliu selectas -->
				<select name="country" class="form-control mt-3">
					<?php foreach($countries as $country) : ?>
                        <?php if($country->getID() == $city->getCountry()) : ?>
                            <option selected value="<?php echo $country->getID(); ?>">
                                <?php echo $country->getName(); ?>
                            </option>
						<?php else: ?>
                            <option value="<?php echo $country->getID(); ?>">
                                <?php echo $country->getName(); ?>
                            </option>
                        <?php endif; ?>
					<?php endforeach; ?>
				</select>
                <input type="hidden" name="id" value="<?php echo $city->getID(); ?>" />
				<input type="submit" class="btn btn-success mt-3" value="Prideti miesta"/>
			</form>
		</div>
	</div>
</div>

<?php include "../countries/footer.php";?>
