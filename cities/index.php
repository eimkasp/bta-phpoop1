<?php include "../countries/header.php";



// Gauname visu saliu informacija
$cities = City::getAll();
?>

<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<h1>Miestu sarasas (<?php echo City::$count; ?>)</h1>

			<ul>
				<?php foreach ( $cities as $city ) : ?>
					<li>
						<a href="show.php?id=<?php echo $city->getID(); ?>">
							<?php echo $city->name; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
<?php include "../countries/footer.php"; ?>




