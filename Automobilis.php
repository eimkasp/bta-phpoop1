<?php

class Automobilis {

	private $spalva;
	private $numeris; // null

	protected $degaluKiekis = 0;

	function __construct($spalva, $numeris, $degaluKiekis = 5) {
		$this->spalva = $spalva;
		$this->numeris = $numeris;
		$this->degaluKiekis = $degaluKiekis;
		echo "sukurtas automobilis: " . $this->numeris;
	}

	function __toString() {
		return "Automobilis: " . $this->numeris . " " . $this->spalva;
	}

	function getSpalva() {
		return $this->spalva;
	}

	function setSpalva($spalva) {
		$this->spalva = $spalva;
	}

	public function PripiltiDegalu( $kiekis ) {
		$this->degaluKiekis += $kiekis;
	}

	public function DegaluLikutis() {
		return $this->degaluKiekis;
	}
}