<?php

class MySQL {
	private static $servername = "localhost";
	private static $username = "root";
	private static $password = "root";
	private static $dbName = "phpoop1";

	private static $link; //null

	private static function connect() {
		try {
			// Duombazes prisijungimo objektas
			$database = new PDO( "mysql:host=" . MySQL::$servername .";dbname=". MySQL::$dbName, MySQL::$username, MySQL::$password );
			// set the PDO error mode to exception
			$database->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

			// Nustatome utf8 koduote
			$database->exec("SET NAMES 'utf8';");

			MySQL::$link = $database;
		} catch ( PDOException $e ) {
			echo "Connection failed: " . $e->getMessage();
		}
	}

	static function select($query) {
		if(MySQL::$link === null) {
			MySQL::connect();
		}

		// Sukuriu uzklausos kintamaji
		$query = MySQL::$link->prepare($query);

		// Ivykdome uzklausa
		$query->execute();

		$results = $query->fetchAll(PDO::FETCH_ASSOC);

		return $results;
	}

	static function query($query) {
		if(MySQL::$link === null) {
			MySQL::connect();
		}

		// Sukuriu uzklausos kintamaji
		$query = MySQL::$link->prepare($query);

		// Ivykdome uzklausa
		$query->execute();


	}

	static function delete($query) {
		if(MySQL::$link === null) {
			MySQL::connect();
		}

		// Sukuriu uzklausos kintamaji
		$query = MySQL::$link->prepare($query);

		// Ivykdome uzklausa
		$query->execute();
	}
}