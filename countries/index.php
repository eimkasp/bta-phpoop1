<?php include "header.php";

// Gauname visu saliu informacija
$countries = Country::getAll();
?>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h1>Valstybiu sarasas (<?php echo Country::$count; ?>)</h1>

            <ul>
				<?php foreach ( $countries as $country ) : ?>
                    <li>
                        <a href="show.php?id=<?php echo $country->getID(); ?>">
							<?php echo $country->name; ?>
                        </a>
                    </li>
				<?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>




