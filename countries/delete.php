<?php

include "MySQL.php";
include "Country.php";

$id = $_GET['id'];

$country = Country::find($id);

$country->delete();

// Redirect to index
header("Location: http://localhost/phpoop/countries");