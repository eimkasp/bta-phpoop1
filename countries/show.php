<?php
include "header.php";
$id = $_GET['id'];

$country = Country::find($id);

?>

<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<h1><?php echo $country->getName(); ?></h1>

			<div>
				Code: <?php echo $country->getCode(); ?>
			</div>

			<?php if($country->getSurfaceArea()) : ?>
				<div>
					Surface area: <?php echo $country->getSurfaceArea(); ?>
				</div>
			<?php endif;?>
		</div>
		<div class="col-sm-4">
			<a href="delete.php?id=<?php echo $country->getID(); ?>" class="btn btn-danger">
				Trinti
			</a>
		</div>
	</div>
</div>

<?php include "footer.php"; ?>