<?php

include "Country.php";
include "../cities/City.php";
include "MySQL.php";
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<ul>
    <li>
        <a href="../countries/index.php">Valstybiu sarasas</a>
    </li>
    <li>
        <a href="../cities/index.php">Miestu sarasas</a>
    </li>
    <li>
        <a href="../cities/create.php">Prideti miesta</a>
    </li>
</ul>