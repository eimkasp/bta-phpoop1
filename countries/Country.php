<?php

class Country {

	private $id;
	private $code;
	public $name;
	private $surfaceArea;
	private $database;

	public static $count = 0;
	public static $all = [];

	function __construct($code, $name, $id, $surfaceArea = 0) {
		$this->code = $code;
		$this->name = $name;
		$this->id = $id;
		$this->surfaceArea = $surfaceArea;

		Country::$count++;
	}

	function getName() {
		return $this->name;
	}

	function getSurfaceArea() {
		return $this->surfaceArea;
	}

	function setName($name) {
		$this->name = $name;
	}

	function getCode() {
		return $this->code;
	}

	function getID() {
		return $this->id;
	}

	static function find($id) {
		$results = MySQL::select("SELECT * from country WHERE id = $id");
		$country = new Country($results[0]['code'], $results[0]['name'], $results[0]['id'], $results[0]['surfaceArea']);

		return $country;
	}

	static function getAll() {

		/* Prisijungiame prie duombazes */
		$results = MySQL::select("SELECT * from country");


		foreach ($results as $result) {
			// Kiekvienai grazintai mysql eilutei, sukuriame country tipo objekta
			$country = new Country($result['code'], $result['name'], $result['id'], $result['surfaceArea']);
			array_push(Country::$all, $country);
		}

		return Country::$all;
	}

	function delete() {
		$query = MySQL::delete("DELETE from country WHERE id = $this->id");
	}


	/*  Padaryti funkcija, kad atnaujinus salies duomenis,
		jie butu pakeiciami ir duombazeje
	*/
}